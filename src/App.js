import React, { useState, useEffect } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Frase from "./components/Frase";
import Spinner from "./components/Spinner";

function App() {
  const [frase, setFrase] = useState({});
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    consultarAPI();
  }, []);

  const consultarAPI = async () => {
    setLoader(true);
    const api = await fetch("https://thesimpsonsquoteapi.glitch.me/quotes"); //Peticion get
    const resp = await api.json();
    console.log(api);
    console.log(resp[0]);
    setTimeout( () => {
      setFrase(resp[0]);
      setLoader(false);
    }, 2000)
    
  };

  //Operador ternario
  const cargarComponente = loader ? (
    <Spinner></Spinner>
  ) : (
    <Frase frase={frase}></Frase>
  );

  return (
    <section className="container text-center my-5">
      <article className="d-flex flex-column align-items-center">
        <img
          src={process.env.PUBLIC_URL + "/logos.png"}
          className="w-50"
          alt="imagen principal de los simpsons"
        ></img>
        <button
          className="btn btn-warning text-dark w-50 shadow my-5"
          onClick={() => consultarAPI()}
        >
          Obtener frase
        </button>
      </article>
      {cargarComponente};
    </section>
  );
}

export default App;
