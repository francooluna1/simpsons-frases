import React from "react";

const Frase = (props) => {
  return (
    <div className="card mb-3">
      <div className="row no-gutters">
        <div className="col-md-4 p-3">
          <img src={props.frase.image} className="card-img" alt={props.frase.character}/>
        </div>
        <div className="col-md-8 d-flex justify-content-center align-items-center">
          <div className="card-body">
            <h5 className="card-title">{props.frase.character}</h5>
            <p className="card-text text-center">{props.frase.quote}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Frase;
